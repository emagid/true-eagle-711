<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	    <meta name = "apple-mobile-web-app-capable" content = "yes" />
		<meta name="google-site-verification" content="KJ5zxDYTDtIcIxId3dCgozvL1KynRVcy7wtd8IyB0RA" />
	    <title>True Eagle</title>
	    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">
	    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

	    <meta property="og:title" content="<?=SITE_NAME?>" />
	    <meta property="og:type" content="website" /> 
	    <meta property="og:url" content="<?=SITE_URL?>" />
	    
 
	    <? require_once('includes.php'); ?>
	</head>

	<body class="<?=$this->emagid->route['controller']?>_<?=$this->emagid->route['action']?>">
		<?display_notification();?>
		<? require_once('header.php'); ?>

		<div class = "modal_overlay"></div>
		<div id="primary_page_controller_wrapper" class="blur_on_overlay">
			<?php $emagid->controller->renderBody($model);?>
	    </div>
        
        <script type='text/javascript'>

        </script>
	</body>
</html>