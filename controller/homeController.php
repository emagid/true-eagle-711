<?php

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    function about(Array $params = []){
        $this->loadView($this->viewData);
    }

    function trip(Array $params = []){
        $this->loadView($this->viewData);
    }

    function brand(Array $params = []){
        $this->loadView($this->viewData);
    }

    function promotions(Array $params = []){
        $this->loadView($this->viewData);
    }

    function trip_entry_post(){
        $response = ['status'=>false, 'msg'=>'failed'];
        header('Content-Type: application/json');

        if(isset($_POST['email']) && isset($_POST['name']) && $_POST['email'] != '' && $_POST['name'] != ''){
            $email = $_POST['email'];            
            if(\Model\Trip_Entry::getItem(null,['where'=>"email = '$email'"]) == ''){
                $new_entry = new \Model\Trip_Entry($_POST);
                $new_entry->save();
                if($new_entry->save()){
                    $response['status'] = true;
                    $response['msg'] = "success";
                }
            }
            else{
                $response['status'] = false;
                $response['msg'] ="failed";
            }
            echo json_encode($response);
        }
    }


}