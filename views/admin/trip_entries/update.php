<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
     
        </ul>
        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->trip_entry->id; ?>" />
                <input name="token" type="hidden" value="<?php echo get_token();?>" />
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->textBoxFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <?php echo $model->form->textBoxFor("email"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 