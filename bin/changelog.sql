-- TRUNCATE public.product_images RESTART IDENTITY;
-- TRUNCATE public.jewelry RESTART IDENTITY;
-- TRUNCATE public.product RESTART IDENTITY;
-- TRUNCATE public.wishlist RESTART IDENTITY;
-- TRUNCATE public.wholesale_items RESTART IDENTITY;
-- TRUNCATE public.transaction RESTART IDENTITY;
-- TRUNCATE public.product_map RESTART IDENTITY;
-- TRUNCATE public.order_product_option RESTART IDENTITY;
-- TRUNCATE public.ring_material RESTART IDENTITY;
-- TRUNCATE public.address RESTART IDENTITY;
-- TRUNCATE public.page RESTART IDENTITY;
-- TRUNCATE public.coupon RESTART IDENTITY;
-- TRUNCATE public.user_favorite RESTART IDENTITY;
-- TRUNCATE public.wedding_band RESTART IDENTITY;
-- TRUNCATE public.order_products RESTART IDENTITY;
-- TRUNCATE public.category RESTART IDENTITY;
-- TRUNCATE public.ring RESTART IDENTITY;
-- TRUNCATE public.wholesale RESTART IDENTITY;
-- TRUNCATE public.banner RESTART IDENTITY;
-- TRUNCATE public.shipping_method RESTART IDENTITY;
-- TRUNCATE public.payment_profiles RESTART IDENTITY;
-- TRUNCATE public.contact RESTART IDENTITY;
-- TRUNCATE public.product_categories RESTART IDENTITY;
-- TRUNCATE public.credit RESTART IDENTITY;
-- TRUNCATE public.order RESTART IDENTITY;


-- October 10

ALTER TABLE public.contact ADD COLUMN donation NUMERIC(10,2);
ALTER TABLE public.contact ADD COLUMN form INTEGER;

-- October 11

CREATE SEQUENCE public.snapshot_contact_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.snapshot_contact
(
  id integer NOT NULL DEFAULT nextval('snapshot_contact_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying COLLATE pg_catalog."default",
  contact_id integer NOT NULL,
  CONSTRAINT snapshot_contact_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE SEQUENCE public.gif_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.gif
(
  id integer NOT NULL DEFAULT nextval('gif_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying COLLATE pg_catalog."default",
  frames character varying COLLATE pg_catalog."default",
  contact_id integer NOT NULL,
  CONSTRAINT gif_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- October 12

ALTER TABLE public.snapshot_contact ADD COLUMN in_slider SMALLINT DEFAULT 0;
ALTER TABLE public.gif ADD COLUMN in_slider SMALLINT DEFAULT 0;

-- February 28

CREATE SEQUENCE public.question_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.question
(
    id integer NOT NULL DEFAULT nextval('question_id_seq'::regclass),
    active smallint NOT NULL DEFAULT 1,
    insert_time timestamp without time zone NOT NULL DEFAULT now(),
    text character varying COLLATE pg_catalog."default",
    failure_text character varying COLLATE pg_catalog."default",
    image character varying COLLATE pg_catalog."default",
    CONSTRAINT question_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE SEQUENCE public.answer_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.answer
(
    id integer NOT NULL DEFAULT nextval('answer_id_seq'::regclass),
    active smallint NOT NULL DEFAULT 1,
    insert_time timestamp without time zone NOT NULL DEFAULT now(),
    text character varying COLLATE pg_catalog."default",
    failure_text character varying COLLATE pg_catalog."default",
    image character varying COLLATE pg_catalog."default",
    CONSTRAINT answer_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- September 20

ALTER TABLE public.answer ADD COLUMN question_id INTEGER;
ALTER TABLE public.answer ADD COLUMN featured_image character varying COLLATE pg_catalog."default";
ALTER TABLE public.answer DROP COLUMN failure_text;
ALTER TABLE public.answer DROP COLUMN image;


ALTER TABLE public.question ADD COLUMN correct_answer_id INTEGER;


ALTER TABLE public.question ADD COLUMN display_order INTEGER;
ALTER TABLE public.answer ADD COLUMN display_order INTEGER;

-- June 8, 2018

CREATE SEQUENCE public.trivia_entry_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.trivia_entry
(
    id integer NOT NULL DEFAULT nextval('trivia_entry_id_seq'::regclass),
    active smallint NOT NULL DEFAULT 1,
    insert_time timestamp without time zone NOT NULL DEFAULT now(),
    name character varying COLLATE pg_catalog."default",
    email character varying COLLATE pg_catalog."default",
    company character varying COLLATE pg_catalog."default",
    CONSTRAINT trivia_entry_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--July 26, 2018


CREATE TABLE public.trip_entry
(
    id serial,
    active smallint NOT NULL DEFAULT 1,
    insert_time timestamp without time zone NOT NULL DEFAULT now(),
    name character varying COLLATE pg_catalog."default",
    email character varying COLLATE pg_catalog."default",
    CONSTRAINT trip_entry_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;