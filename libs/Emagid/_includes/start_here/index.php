<?php 

error_reporting(E_ALL);

require_once("lib/Emagid/emagid.php");
require_once("conf/emagid.conf.php");

$emagid = new \Emagid\Emagid($emagid_conf);

$emagid->loadMvc();
