<main>
    <section class="product_page about_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
            <a class='arrow_back' href="/"><img src="<?=FRONT_ASSETS?>img/arrow.png"></a>
        </header>

        <div class='banner'>
        	<div class='banner_overlay'>
        		<img src="<?=FRONT_ASSETS?>img/about_logo.png">
        		<h1>Long Island’s Most Secure, <br>Fully-Redundant Data Center</h1>
        		<div>
        			<!-- <img src="<?=FRONT_ASSETS?>img/about_logo.png"> -->
        		</div>
        	</div>
        </div>

        <div class='content' style='padding-bottom: 25px;'>
        	<h2>Datacenter</h2>
        	<p>Enjoy the performance and flexibility that comes with hosting your company’s most mission-critical infrastructure at Long Island’s proven choice for redundancy and security: NY1 by Webair. Our Tier III-rated data center is the premier facility east of New York City for enterprise colocation, private and hybrid cloud solutions, and managed services.</p>
        </div>

        <div class='content' style='padding-top: 0;'>
            <div class='logos'>
                <img src="<?=FRONT_ASSETS?>img/about_logo1.jpg">
                <img src="<?=FRONT_ASSETS?>img/about_logo2.png">
                <img src="<?=FRONT_ASSETS?>img/about_logo3.png">
            </div>
        </div>

    	<div class='darken'>
        	<ul class='bullets'>
        		<li>NY1 is a Tier III-rated, SSAE16, HIPAA, CJIS, PCI-DSS, and Open-IX certified data center</li>
        		<li>N+1 or better redundant cooling, power, and connectivity</li>
        		<li>Carrier-neutral facility, with multiple entry points and carriers</li>
        		<li>25,000 sq. ft. of raised floor space, with a load capacity of 300 lbs./ sq. ft.</li>
        	</ul>
    	</div>

    </section>
</main>

 