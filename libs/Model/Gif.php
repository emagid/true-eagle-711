<?php

namespace Model;

class Gif extends \Emagid\Core\Model
{

    static $tablename = "gif";

    public static $fields = [
        'image',
        'frames',
        'contact_id',
        'in_slider'
    ];

    /**
     * checks that the image exists in the path
     *
     * @param type $size : size of an image to check if that size exists
     * @return boolean: true if file exists, false otherwise
     */

    public static function slider(){
        $gifs = [];
        foreach( self::getList(['where'=>"in_slider = 1"]) as $gif){
//            $gifs[] = $gif->image;
            $gifs[] = UPLOAD_URL.'Gifs'.DS.$gif->image;
        }
        return json_encode($gifs);
    }

    public function exists_image()
    {
        if ($this->image != "" && file_exists(UPLOAD_PATH . 'Gifs' . DS . $this->image)) {
            return true;
        }
        return false;
    }

    /**
     * builds url link to the image in the specified path.
     *
     * @param type $size : optional size to get the image with that specific size
     * @return type: url to image
     */
    public function get_image_url()
    {
        return UPLOAD_URL . 'Gifs/' . $this->image;
    }

    public function getProductImage($id, $productType){
        $productMap = new Product_Map();
        if($product = $productMap->getMap($id, $productType)->id) {
            return self::getList(['where' => "product_map_id = $product", 'orderBy' => 'display_order', 'sort' => 'DESC']);
        } else {
            return '';
        }
    }
}

