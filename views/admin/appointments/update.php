<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#details-info-tab" aria-controls="categories" role="tab" data-toggle="tab">Details</a></li>
		<!--		<li role="presentation"><a href="#patients-info-tab" aria-controls="general" role="tab" data-toggle="tab">Patients</a></li>-->
		<!--		<li role="presentation"><a href="#appointment-tab" aria-controls="seo" role="tab" data-toggle="tab">Appointments</a></li>-->
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="details-info-tab">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<h4>Appointment Information</h4>
						<div class="form-group">
							<label>Office:</label>
							<p><span class="office-name" data-office="<?=$model->appointment->O ?>"><?=$model->appointment->O ?></span></p>
						</div>
						<div class="form-group">
							<label>Patient Name:</label>
							<? if(isset($model->patient)) {?>
								<p><?="{$model->patient->LN}, {$model->patient->FN}".(!empty($model->patient->MI) ? ' '.$model->patient->MI : '' )?></p>
							<?} else {?>
								<p>New Patient</p>
							<?} ?>
						</div>
						<div class="form-group">
							<label>Doctor Name:</label>
							<p><?=$model->appointment->PRN?></p>
						</div>
						<div class="form-group">
							<label>Scheduled on:</label>
							<p><?=date('m/d/y',strtotime($model->appointment->APD))?></p>
							<p><?=date('h:m A',strtotime($model->appointment->APD))?></p>
						</div>
						<div class="form-group">
							<label>Scheduled length:</label>
							<p><?=$model->appointment->APL." Minutes"?></p>
						</div>
						<div class="form-group">
							<label>Created On:</label>
							<p><?=date('m/d/y',strtotime($model->appointment->CBO))?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>