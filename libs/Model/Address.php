<?php
namespace Model;

class Address extends \Emagid\Core\Model {
	static $tablename = "address";
	public static $fields = [
		'user_id',	
		'is_default'=>['type'=>'boolean'],
		'first_name',
		'last_name',
		'phone',
		'address',
		'address2',
		'city',
		'state'=>['required'=>true],
		'country',
		'zip'=>['required'=>true],
		'label'
	];
}