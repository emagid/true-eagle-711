<?php

use Twilio\Rest\Client;

class contactController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Contact | Webair";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $response = ['status'=>false,
                     'msg'=>'failed to Send'];


        $obj = \Model\Contact::loadFromPost();
        if(count($_POST['email']) > 0){
            $obj->email = $_POST['email'][0];
        } else {
            $obj->email = '';
        }
        if(count($_POST['phone']) > 0){
            $obj->phone = $_POST['phone'][0];
        } else {
            $obj->phone = '';
        }
        if($obj->save()){
//          $n = new \Notification\MessageHandler('We will contact you shortly.');
//          $_SESSION["notification"] = serialize($n);
            $imgs = [];
            $gif = [];
            $email_image = null;
            if(isset($_POST['image'])){
                $img = $_POST['image'];
                $image = \Model\Snapshot_Contact::getItem($img);
                $image->contact_id = $obj->id;
                if($image->save()){
                    $response['image']=$image;
                    $email_image = 'https://'.$_SERVER['SERVER_NAME'].$image->get_image_url();
                }
            } else if(isset($_POST['gif'])){
                $_gif = $_POST['gif'];
                $gif = \Model\Gif::getItem($_gif);
                $gif->contact_id = $obj->id;
                $gif->image = str_replace('\\','/',$gif->image);
                if($gif->save()){
                    $response['gif'] = $gif;
                    $email_image = 'https://'.$_SERVER['SERVER_NAME'].$gif->get_image_url();
                }
            }

            $email = new \Email\MailMaster();
            $sid = TWILIO_SID;
            $token = TWILIO_TOKEN;
            $client = new Client($sid, $token);

            $email_responses = [];
            $phone_responses = [];

            foreach ($_POST['email'] as $_email){
                if($_email == '') continue;
                if($email_image == null)
                    $eimage = '<p>Thanks for attending!</p>';
                else
                    $eimage = $email_image;
                $mergeTags = [
                    'CONTENT'=>"<img style='max-width: 728px; width: 100%;'src='$eimage'>"
                ];
                $email_responses[$_email] = $email->setTo(['email' => $_email, 'name' => ucwords($obj->name), 'type' => 'to'])->setSubject('Thank You!')->setTemplate('true-eagle-demo')->setMergeTags($mergeTags)->send();
            }
            // foreach ($_POST['phone'] as $phone){
            //     if($phone == '') continue;
            //     $phone_responses[$phone] = $client->messages
            //         ->create(
            //             $phone,
            //             array(
            //                 "from" => TWILIO_NUMBER,
            //                 "body" => "Here's your picture!! ". $email_image,
            //             )
            //         );
            // }
            foreach ($_POST['phone'] as $phone){

                if($phone == '') continue;
                try{
                    $phone_number = $client->lookups->v1->phoneNumbers($phone)
                        ->fetch(array(
                            'addons' => "whitepages_pro_caller_id"
                            )
                        );

                    if($phone_number->addOns['status'] == 'successful'){
                        $phone_responses[$phone] = $client->messages
                        ->create(
                            $phone,
                            array(
                                "from" => TWILIO_NUMBER,
                                "body" => "Here's your picture!! ". $email_image,
                            )
                        );
                    }
                    else{
                        $n = new \Notification\ErrorHandler("SOmething went wrong..Please Try again!!");
                            $_SESSION['notification'] = serialize($n);
                    }

                }catch(Exception $e){
                    // if($e->getStatusCode() == 404) {
                    //     $n = new \Notification\ErrorHandler("<p class='error'>Please Enter Valid Phone Number</p>");
                    //     $_SESSION['notification'] = serialize($n);
                    // }
                    // else{
                    //     $n = new \Notification\ErrorHandler("<p class='error'>Something went wrong...Please try agin with valid phone number!!</p>");
                    //     $_SESSION['notification'] = serialize($n);
                    // }
                }
            }


            $response['phones'] = $phone_responses;
            $response['emails'] = $email_responses;
            $response['status'] = true;
            $response['msg'] = 'Success';
            $response['contact'] = $obj;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function make_gif_post(){
        $response = ['status'=>true];
        if(isset($_POST['images'])){
            $imgs = [];
            foreach($_POST['images'] as $img){

                $image = $this->save_img($img);
                $imgs[] = $image;
            }
            $response['images'] = $imgs;

            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function make_frame_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');
        if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image'] = $image;
        } else {
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function save_img_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');

        if(isset($_POST['gif'])){
            $gif = new \Model\Gif();
            $gif->frames = json_encode($_POST['images']);
            foreach($_POST['images'] as $img){
//                $_img = \Model\Snapshot_Contact::getItem($img);
                \Model\Snapshot_Contact::delete($img);
            }

            $img = $_POST['gif'];
            $img = str_replace('data:image/gif;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $fileName = uniqid().'.gif';
            $file = UPLOAD_PATH.'Gifs'.DS.$fileName;

            if (!is_dir(UPLOAD_PATH.'Gifs'.DS)){
                mkdir(UPLOAD_PATH.'Gifs'.DS, 0777, true);
            }

            $success = file_put_contents($file, $data);


//                $gif->image = $fileName;
            $gif->image = $fileName;
            $gif->contact_id = 0;
            $gif->frames = json_encode($_POST['images']);
            if($gif->save()){
                $response['gif'] = $gif;
            } else {
                $response['status'] = false;
                $response['errors'] = $gif->errors;
                $response['msg'] = "Gif Save Failed";
            }
        } else if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image']=$image;
        }
        echo json_encode($response);
    }

    public function save_img($_img, $obj = null){
        $img = $_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $fileName = uniqid().'.png';
        $file = UPLOAD_PATH.'Snapshots'.DS.$fileName;

        if (!is_dir(UPLOAD_PATH.'Snapshots'.DS)){
            mkdir(UPLOAD_PATH.'Snapshots'.DS, 0777, true);
        }

        $success = file_put_contents($file, $data);

        $image = new \Model\Snapshot_Contact();
        if($obj) $image->contact_id = $obj->id;
        else $image->contact_id = 0;
        $image->image = $fileName;
        $image->save();

        if($obj){
            $obj->save();
        }
        return($image);
    }
}